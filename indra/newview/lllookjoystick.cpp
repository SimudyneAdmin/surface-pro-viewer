
#include "llviewerprecompiledheaders.h"

#include "lllookjoystick.h"

// Library includes
#include "indra_constants.h"
#include "llparcel.h"

// Viewer includes

#include "llagent.h"
#include "llagentcamera.h"
#include "llvoavatarself.h" // to check gAgentAvatarp->isSitting()
#include "llbutton.h"
#include "llfirstuse.h"
#include "llfloaterreg.h"
#include "llhints.h"
#include "lljoystickbutton.h"
#include "lluictrlfactory.h"
#include "llviewerwindow.h"
#include "llviewercontrol.h"
#include "llselectmgr.h"
#include "lltoolbarview.h"
#include "llviewerparcelmgr.h"
#include "llviewerregion.h"
#include "lltooltip.h"

//
// Constants
//

const F32 MOVE_BUTTON_DELAY = 0.0f;
const F32 YAW_NUDGE_RATE = 0.05f;	// fraction of normal speed
const F32 PITCH_NUDGE_RATE = 0.05f;	// fraction of normal speed
const F32 NUDGE_TIME = 0.25f;		// in seconds

//
// Member functions
//

// protected
LLLookJoystick::LLLookJoystick(const LLSD& key)
:	LLFloater(key),
	mLookLeftButton(NULL), 
	mLookRightButton(NULL),
	mLookUpButton(NULL),
	mLookDownButton(NULL),
	mModeActionsPanel(NULL),
	mCurrentMode(MM_WALK)
{
	setCanDrag(FALSE);
	setVisible(TRUE);
	setFloatBackgroundVisible(true);
}

LLLookJoystick::~LLLookJoystick()
{
	// Ensure LLPanelStandStopFlying panel is not among floater's children. See EXT-8458.
	setVisible(FALSE);

	// Otherwise it can be destroyed and static pointer in LLPanelStandStopFlying::getInstance() will become invalid.
	// Such situation was possible when LLFloaterReg returns "dead" instance of floater.
	// Should not happen after LLFloater::destroy was modified to remove "dead" instances from LLFloaterReg.
}

// virtual
BOOL LLLookJoystick::postBuild()
{
	updateTransparency(TT_ACTIVE); // force using active floater transparency (STORM-730)
	
	// Code that implements floater buttons toggling when user moves via keyboard is located in LLAgent::propagate()

	mLookLeftButton = getChild<LLButton>("look left btn"); 
	mLookLeftButton->setHeldDownDelay(MOVE_BUTTON_DELAY);
	mLookLeftButton->setHeldDownCallback(boost::bind(&LLLookJoystick::lookLeft, this));
	mLookRightButton = getChild<LLButton>("look right btn"); 
	mLookRightButton->setHeldDownDelay(MOVE_BUTTON_DELAY);
	mLookRightButton->setHeldDownCallback(boost::bind(&LLLookJoystick::lookRight, this));

	mLookUpButton = getChild<LLButton>("look up btn"); 
	mLookUpButton->setHeldDownDelay(MOVE_BUTTON_DELAY);
	mLookUpButton->setHeldDownCallback(boost::bind(&LLLookJoystick::lookUp, this));
	mLookDownButton = getChild<LLButton>("look down btn"); 
	mLookDownButton->setHeldDownDelay(MOVE_BUTTON_DELAY);
	mLookDownButton->setHeldDownCallback(boost::bind(&LLLookJoystick::lookDown, this));


	mModeActionsPanel = getChild<LLPanel>("panel_modes");

	LLButton* btn;
	btn = getChild<LLButton>("mode_walk_btn");
	btn->setCommitCallback(boost::bind(&LLLookJoystick::onWalkButtonClick, this));

	btn = getChild<LLButton>("mode_run_btn");
	btn->setCommitCallback(boost::bind(&LLLookJoystick::onRunButtonClick, this));

	btn = getChild<LLButton>("mode_fly_btn");
	btn->setCommitCallback(boost::bind(&LLLookJoystick::onFlyButtonClick, this));

	initModeTooltips();

	initModeButtonMap();

	initMovementMode();

	LLViewerParcelMgr::getInstance()->addAgentParcelChangedCallback(LLLookJoystick::sUpdateFlyingStatus);

	setVisible(TRUE);

	return TRUE;
}

// *NOTE: we assume that setVisible() is called on floater close.
// virtual
void LLLookJoystick::setVisible(BOOL visible)
{
	// Do nothing with Stand/Stop Flying panel in excessive calls of this method.
	if (getVisible() == visible)
	{
		LLFloater::setVisible(visible);
		return;
	}

	if (visible)
	{
		LLFirstUse::notMoving(false);
	}

	LLFloater::setVisible(visible);
}

// static 
F32 LLLookJoystick::getYawRate( F32 time )
{
	if( time < NUDGE_TIME )
	{
		F32 rate = YAW_NUDGE_RATE + time * (1 - YAW_NUDGE_RATE)/ NUDGE_TIME;
		return rate;
	}
	else
	{
		return 1.f;
	}
}

F32 LLLookJoystick::getPitchRate( F32 time )
{
	if( time < NUDGE_TIME )
	{
		F32 rate = PITCH_NUDGE_RATE + time * (1 - PITCH_NUDGE_RATE)/ NUDGE_TIME;
		return rate;
	}
	else
	{
		return 1.f;
	}
}

// static 
void LLLookJoystick::setFlyingMode(BOOL fly)
{
	LLLookJoystick* instance = LLFloaterReg::findTypedInstance<LLLookJoystick>("right_look_joystick");
	if (instance)
	{
		instance->setFlyingModeImpl(fly);
		LLVOAvatarSelf* avatar_object = gAgentAvatarp;
		bool is_sitting = avatar_object
			&& (avatar_object->getRegion() != NULL)
			&& (!avatar_object->isDead())
			&& avatar_object->isSitting();
		instance->showModeButtons(!fly && !is_sitting);
	}
}
//static
void LLLookJoystick::setAlwaysRunMode(bool run)
{
	LLLookJoystick* instance = LLFloaterReg::findTypedInstance<LLLookJoystick>("right_look_joystick");
	if (instance)
	{
		instance->setAlwaysRunModeImpl(run);
	}
}

void LLLookJoystick::setFlyingModeImpl(BOOL fly)
{
	updateButtonsWithMovementMode(fly ? MM_FLY : (gAgent.getAlwaysRun() ? MM_RUN : MM_WALK));
}

void LLLookJoystick::setAlwaysRunModeImpl(bool run)
{
	if (!gAgent.getFlying())
	{
		updateButtonsWithMovementMode(run ? MM_RUN : MM_WALK);
	}
}

//static
void LLLookJoystick::setSittingMode(BOOL bSitting)
{
	enableInstance(!bSitting);
}

// protected 
void LLLookJoystick::lookLeft()
{
	if (gAgentCamera.cameraProactIVEView()){
		F32 time = mLookLeftButton->getHeldDownTime();
		gAgent.moveYaw( getYawRate( time ) );
	} else {
		gAgentCamera.cameraOrbitAround(0.05);
	}
}

// protected
void LLLookJoystick::lookRight()
{
	if (gAgentCamera.cameraProactIVEView()){
		F32 time = mLookRightButton->getHeldDownTime();
		gAgent.moveYaw( -getYawRate( time ) );
	} else {
		gAgentCamera.cameraOrbitAround(-0.05);
	}
}

// protected
void LLLookJoystick::lookUp()
{
	if (gAgentCamera.cameraProactIVEView()){
		F32 time = mLookUpButton->getHeldDownTime();
		gAgent.movePitch( -getPitchRate( time ) );} 
	else {
		gAgentCamera.cameraOrbitOver(-0.05);
	}
}

// protected
void LLLookJoystick::lookDown()
{
	if (gAgentCamera.cameraProactIVEView()){
		F32 time = mLookDownButton->getHeldDownTime();
		gAgent.movePitch( getPitchRate( time ) );
	} else {
		gAgentCamera.cameraOrbitOver(0.05);
	}
}

//////////////////////////////////////////////////////////////////////////
// Private Section:
//////////////////////////////////////////////////////////////////////////

void LLLookJoystick::onWalkButtonClick()
{
	setMovementMode(MM_WALK);
}
void LLLookJoystick::onRunButtonClick()
{
	setMovementMode(MM_RUN);
}
void LLLookJoystick::onFlyButtonClick()
{
	setMovementMode(MM_FLY);
}

void LLLookJoystick::setMovementMode(const EMovementMode mode)
{
	mCurrentMode = mode;
	gAgent.setFlying(MM_FLY == mode);

	// attempts to set avatar flying can not set it real flying in some cases.
	// For ex. when avatar fell down & is standing up.
	// So, no need to continue processing FLY mode. See EXT-1079
	if (MM_FLY == mode && !gAgent.getFlying())
	{
		return;
	}

	switch (mode)
	{
	case MM_RUN:
		gAgent.setAlwaysRun();
		gAgent.setRunning();
		break;
	case MM_WALK:
		gAgent.clearAlwaysRun();
		gAgent.clearRunning();
		break;
	default:
		//do nothing for other modes (MM_FLY)
		break;
	}
	// tell the simulator.
	gAgent.sendWalkRun(gAgent.getAlwaysRun());
	
	updateButtonsWithMovementMode(mode);

	bool bHideModeButtons = MM_FLY == mode
		|| (isAgentAvatarValid() && gAgentAvatarp->isSitting());

	showModeButtons(!bHideModeButtons);

}

void LLLookJoystick::updateButtonsWithMovementMode(const EMovementMode newMode)
{
	setModeTooltip(newMode);
	setModeButtonToggleState(newMode);
	setModeTitle(newMode);
}

void LLLookJoystick::initModeTooltips()
{
	control_tooltip_map_t walkTipMap;
	walkTipMap.insert(std::make_pair(mLookUpButton, getString("jump_tooltip")));
	walkTipMap.insert(std::make_pair(mLookDownButton, getString("crouch_tooltip")));
	mModeControlTooltipsMap[MM_WALK] = walkTipMap;

	control_tooltip_map_t runTipMap;
	runTipMap.insert(std::make_pair(mLookUpButton, getString("jump_tooltip")));
	runTipMap.insert(std::make_pair(mLookDownButton, getString("crouch_tooltip")));
	mModeControlTooltipsMap[MM_RUN] = runTipMap;

	control_tooltip_map_t flyTipMap;
	flyTipMap.insert(std::make_pair(mLookUpButton, getString("fly_up_tooltip")));
	flyTipMap.insert(std::make_pair(mLookDownButton, getString("fly_down_tooltip")));
	mModeControlTooltipsMap[MM_FLY] = flyTipMap;

	setModeTooltip(MM_WALK);
}

void LLLookJoystick::initModeButtonMap()
{
	mModeControlButtonMap[MM_WALK] = getChild<LLButton>("mode_walk_btn");
	mModeControlButtonMap[MM_RUN] = getChild<LLButton>("mode_run_btn");
	mModeControlButtonMap[MM_FLY] = getChild<LLButton>("mode_fly_btn");
}

void LLLookJoystick::initMovementMode()
{
	EMovementMode initMovementMode = gAgent.getAlwaysRun() ? MM_RUN : MM_WALK;
	if (gAgent.getFlying())
	{
		initMovementMode = MM_FLY;
	}
	setMovementMode(initMovementMode);

	if (isAgentAvatarValid())
	{
		showModeButtons(!gAgentAvatarp->isSitting());
	}
}

void LLLookJoystick::setModeTooltip(const EMovementMode mode)
{
	llassert_always(mModeControlTooltipsMap.end() != mModeControlTooltipsMap.find(mode));
	control_tooltip_map_t controlsTipMap = mModeControlTooltipsMap[mode];
	control_tooltip_map_t::const_iterator it = controlsTipMap.begin();
	for (; it != controlsTipMap.end(); ++it)
	{
		LLView* ctrl = it->first;
		std::string tooltip = it->second;
		ctrl->setToolTip(tooltip);
	}
}

void LLLookJoystick::setModeTitle(const EMovementMode mode)
{
	std::string title; 
	switch(mode)
	{
	case MM_WALK:
		title = getString("walk_title");
		break;
	case MM_RUN:
		title = getString("run_title");
		break;
	case MM_FLY:
		title = getString("fly_title");
		break;
	default:
		// title should be provided for all modes
		llassert(false);
		break;
	}
	setTitle(title);
}

//static
void LLLookJoystick::sUpdateFlyingStatus()
{
	LLLookJoystick *floater = LLFloaterReg::findTypedInstance<LLLookJoystick>("right_look_joystick");
	if (floater) floater->mModeControlButtonMap[MM_FLY]->setEnabled(gAgent.canFly());
	
}

void LLLookJoystick::showModeButtons(BOOL bShow)
{
	if (mModeActionsPanel->getVisible() == bShow)
		return;
	mModeActionsPanel->setVisible(bShow);
}

//static
void LLLookJoystick::enableInstance(BOOL bEnable)
{
	LLLookJoystick* instance = LLFloaterReg::findTypedInstance<LLLookJoystick>("right_look_joystick");
	if (instance)
	{
		if (gAgent.getFlying())
		{
			instance->showModeButtons(FALSE);
		}
		else
		{
			instance->showModeButtons(bEnable);
		}
	}
}

void LLLookJoystick::onOpen(const LLSD& key)
{
	if (gAgent.getFlying())
	{
		setFlyingMode(TRUE);
		showModeButtons(FALSE);
	}

	if (isAgentAvatarValid() && gAgentAvatarp->isSitting())
	{
		setSittingMode(TRUE);
		showModeButtons(FALSE);
	}

	sUpdateFlyingStatus();
}

void LLLookJoystick::setModeButtonToggleState(const EMovementMode mode)
{
	llassert_always(mModeControlButtonMap.end() != mModeControlButtonMap.find(mode));

	mode_control_button_map_t::const_iterator it = mModeControlButtonMap.begin();
	for (; it != mModeControlButtonMap.end(); ++it)
	{
		it->second->setToggleState(FALSE);
	}

	mModeControlButtonMap[mode]->setToggleState(TRUE);
}