
#include "llviewerprecompiledheaders.h"

#include "llmovejoystick.h"

// Library includes
#include "indra_constants.h"
#include "llparcel.h"

// Viewer includes

#include "llagent.h"
#include "llagentcamera.h"
#include "llvoavatarself.h" // to check gAgentAvatarp->isSitting()
#include "llbutton.h"
#include "llfirstuse.h"
#include "llfloaterreg.h"
#include "llhints.h"
#include "lljoystickbutton.h"
#include "lluictrlfactory.h"
#include "llviewerwindow.h"
#include "llviewercontrol.h"
#include "llselectmgr.h"
#include "lltoolbarview.h"
#include "llviewerparcelmgr.h"
#include "llviewerregion.h"
#include "lltooltip.h"

//
// Constants
//

const F32 MOVE_BUTTON_DELAY = 0.0f;
const F32 YAW_NUDGE_RATE = 0.05f;	// fraction of normal speed
const F32 NUDGE_TIME = 0.25f;		// in seconds

//
// Member functions
//

// protected
LLMoveJoystick::LLMoveJoystick(const LLSD& key)
:	LLFloater(key),
	mForwardButton(NULL),
	mBackwardButton(NULL),
	mTurnLeftButton(NULL), 
	mTurnRightButton(NULL),
	mMoveUpButton(NULL),
	mMoveDownButton(NULL),
	mModeActionsPanel(NULL),
	mCurrentMode(MM_WALK)
{
	setCanDrag(FALSE);
	setVisible(TRUE);
	setFloatBackgroundVisible(true);
}

LLMoveJoystick::~LLMoveJoystick()
{
	// Ensure LLPanelStandStopFlying panel is not among floater's children. See EXT-8458.
	setVisible(FALSE);

	// Otherwise it can be destroyed and static pointer in LLPanelStandStopFlying::getInstance() will become invalid.
	// Such situation was possible when LLFloaterReg returns "dead" instance of floater.
	// Should not happen after LLFloater::destroy was modified to remove "dead" instances from LLFloaterReg.
}

// virtual
BOOL LLMoveJoystick::postBuild()
{
	updateTransparency(TT_ACTIVE); // force using active floater transparency (STORM-730)
	
	// Code that implements floater buttons toggling when user moves via keyboard is located in LLAgent::propagate()

	mForwardButton = getChild<LLJoystickAgentTurn>("forward btn"); 
	mForwardButton->setHeldDownDelay(MOVE_BUTTON_DELAY);

	mBackwardButton = getChild<LLJoystickAgentTurn>("backward btn"); 
	mBackwardButton->setHeldDownDelay(MOVE_BUTTON_DELAY);

	mSlideLeftButton = getChild<LLJoystickAgentSlide>("move left btn");
	mSlideLeftButton->setHeldDownDelay(MOVE_BUTTON_DELAY);

	mSlideRightButton = getChild<LLJoystickAgentSlide>("move right btn");
	mSlideRightButton->setHeldDownDelay(MOVE_BUTTON_DELAY);

	mTurnLeftButton = getChild<LLButton>("turn left btn"); 
	mTurnLeftButton->setHeldDownDelay(MOVE_BUTTON_DELAY);
	mTurnLeftButton->setHeldDownCallback(boost::bind(&LLMoveJoystick::turnLeft, this));
	mTurnRightButton = getChild<LLButton>("turn right btn"); 
	mTurnRightButton->setHeldDownDelay(MOVE_BUTTON_DELAY);
	mTurnRightButton->setHeldDownCallback(boost::bind(&LLMoveJoystick::turnRight, this));

	mMoveUpButton = getChild<LLButton>("move up btn"); 
	mMoveUpButton->setHeldDownDelay(MOVE_BUTTON_DELAY);
	mMoveUpButton->setHeldDownCallback(boost::bind(&LLMoveJoystick::moveUp, this));

	mMoveDownButton = getChild<LLButton>("move down btn"); 
	mMoveDownButton->setHeldDownDelay(MOVE_BUTTON_DELAY);
	mMoveDownButton->setHeldDownCallback(boost::bind(&LLMoveJoystick::moveDown, this));


	mModeActionsPanel = getChild<LLPanel>("panel_modes");

	LLButton* btn;
	btn = getChild<LLButton>("mode_walk_btn");
	btn->setCommitCallback(boost::bind(&LLMoveJoystick::onWalkButtonClick, this));

	btn = getChild<LLButton>("mode_run_btn");
	btn->setCommitCallback(boost::bind(&LLMoveJoystick::onRunButtonClick, this));

	btn = getChild<LLButton>("mode_fly_btn");
	btn->setCommitCallback(boost::bind(&LLMoveJoystick::onFlyButtonClick, this));

	initModeTooltips();

	initModeButtonMap();

	initMovementMode();

	LLViewerParcelMgr::getInstance()->addAgentParcelChangedCallback(LLMoveJoystick::sUpdateFlyingStatus);

	setVisible(TRUE);

	return TRUE;
}

// *NOTE: we assume that setVisible() is called on floater close.
// virtual
void LLMoveJoystick::setVisible(BOOL visible)
{
	// Do nothing with Stand/Stop Flying panel in excessive calls of this method.
	if (getVisible() == visible)
	{
		LLFloater::setVisible(visible);
		return;
	}

	if (visible)
	{
		LLFirstUse::notMoving(false);
	}

	LLFloater::setVisible(visible);
}

// static 
F32 LLMoveJoystick::getYawRate( F32 time )
{
	if( time < NUDGE_TIME )
	{
		F32 rate = YAW_NUDGE_RATE + time * (1 - YAW_NUDGE_RATE)/ NUDGE_TIME;
		return rate;
	}
	else
	{
		return 1.f;
	}
}


// static 
void LLMoveJoystick::setFlyingMode(BOOL fly)
{
	LLMoveJoystick* instance = LLFloaterReg::findTypedInstance<LLMoveJoystick>("left_joystick");
	if (instance)
	{
		instance->setFlyingModeImpl(fly);
		LLVOAvatarSelf* avatar_object = gAgentAvatarp;
		bool is_sitting = avatar_object
			&& (avatar_object->getRegion() != NULL)
			&& (!avatar_object->isDead())
			&& avatar_object->isSitting();
		instance->showModeButtons(!fly && !is_sitting);
	}
}
//static
void LLMoveJoystick::setAlwaysRunMode(bool run)
{
	LLMoveJoystick* instance = LLFloaterReg::findTypedInstance<LLMoveJoystick>("left_joystick");
	if (instance)
	{
		instance->setAlwaysRunModeImpl(run);
	}
}

void LLMoveJoystick::setFlyingModeImpl(BOOL fly)
{
	updateButtonsWithMovementMode(fly ? MM_FLY : (gAgent.getAlwaysRun() ? MM_RUN : MM_WALK));
}

void LLMoveJoystick::setAlwaysRunModeImpl(bool run)
{
	if (!gAgent.getFlying())
	{
		updateButtonsWithMovementMode(run ? MM_RUN : MM_WALK);
	}
}

//static
void LLMoveJoystick::setSittingMode(BOOL bSitting)
{
	enableInstance(!bSitting);
}

// protected 
void LLMoveJoystick::turnLeft()
{
	F32 time = mTurnLeftButton->getHeldDownTime();
	gAgent.moveYaw( getYawRate( time ) );
}

// protected
void LLMoveJoystick::turnRight()
{
	F32 time = mTurnRightButton->getHeldDownTime();
	gAgent.moveYaw( -getYawRate( time ) );
}

// protected
void LLMoveJoystick::moveUp()
{
	// Jumps or flys up, depending on fly state
	gAgent.moveUp(1);
}

// protected
void LLMoveJoystick::moveDown()
{
	// Crouches or flys down, depending on fly state
	gAgent.moveUp(-1);
}

//////////////////////////////////////////////////////////////////////////
// Private Section:
//////////////////////////////////////////////////////////////////////////

void LLMoveJoystick::onWalkButtonClick()
{
	setMovementMode(MM_WALK);
}
void LLMoveJoystick::onRunButtonClick()
{
	setMovementMode(MM_RUN);
}
void LLMoveJoystick::onFlyButtonClick()
{
	setMovementMode(MM_FLY);
}

void LLMoveJoystick::setMovementMode(const EMovementMode mode)
{
	mCurrentMode = mode;
	gAgent.setFlying(MM_FLY == mode);

	// attempts to set avatar flying can not set it real flying in some cases.
	// For ex. when avatar fell down & is standing up.
	// So, no need to continue processing FLY mode. See EXT-1079
	if (MM_FLY == mode && !gAgent.getFlying())
	{
		return;
	}

	switch (mode)
	{
	case MM_RUN:
		gAgent.setAlwaysRun();
		gAgent.setRunning();
		break;
	case MM_WALK:
		gAgent.clearAlwaysRun();
		gAgent.clearRunning();
		break;
	default:
		//do nothing for other modes (MM_FLY)
		break;
	}
	// tell the simulator.
	gAgent.sendWalkRun(gAgent.getAlwaysRun());
	
	updateButtonsWithMovementMode(mode);

	bool bHideModeButtons = MM_FLY == mode
		|| (isAgentAvatarValid() && gAgentAvatarp->isSitting());

	showModeButtons(!bHideModeButtons);

}

void LLMoveJoystick::updateButtonsWithMovementMode(const EMovementMode newMode)
{
	setModeTooltip(newMode);
	setModeButtonToggleState(newMode);
	setModeTitle(newMode);
}

void LLMoveJoystick::initModeTooltips()
{
	control_tooltip_map_t walkTipMap;
	walkTipMap.insert(std::make_pair(mForwardButton, getString("walk_forward_tooltip")));
	walkTipMap.insert(std::make_pair(mBackwardButton, getString("walk_back_tooltip")));
	walkTipMap.insert(std::make_pair(mSlideLeftButton, getString("walk_left_tooltip")));
	walkTipMap.insert(std::make_pair(mSlideRightButton, getString("walk_right_tooltip")));
	walkTipMap.insert(std::make_pair(mMoveUpButton, getString("jump_tooltip")));
	walkTipMap.insert(std::make_pair(mMoveDownButton, getString("crouch_tooltip")));
	mModeControlTooltipsMap[MM_WALK] = walkTipMap;

	control_tooltip_map_t runTipMap;
	runTipMap.insert(std::make_pair(mForwardButton, getString("run_forward_tooltip")));
	runTipMap.insert(std::make_pair(mBackwardButton, getString("run_back_tooltip")));
	runTipMap.insert(std::make_pair(mSlideLeftButton, getString("run_left_tooltip")));
	runTipMap.insert(std::make_pair(mSlideRightButton, getString("run_right_tooltip")));
	runTipMap.insert(std::make_pair(mMoveUpButton, getString("jump_tooltip")));
	runTipMap.insert(std::make_pair(mMoveDownButton, getString("crouch_tooltip")));
	mModeControlTooltipsMap[MM_RUN] = runTipMap;

	control_tooltip_map_t flyTipMap;
	flyTipMap.insert(std::make_pair(mForwardButton, getString("fly_forward_tooltip")));
	flyTipMap.insert(std::make_pair(mBackwardButton, getString("fly_back_tooltip")));
	flyTipMap.insert(std::make_pair(mSlideLeftButton, getString("fly_left_tooltip")));
	flyTipMap.insert(std::make_pair(mSlideRightButton, getString("fly_right_tooltip")));
	flyTipMap.insert(std::make_pair(mMoveUpButton, getString("fly_up_tooltip")));
	flyTipMap.insert(std::make_pair(mMoveDownButton, getString("fly_down_tooltip")));
	mModeControlTooltipsMap[MM_FLY] = flyTipMap;

	setModeTooltip(MM_WALK);
}

void LLMoveJoystick::initModeButtonMap()
{
	mModeControlButtonMap[MM_WALK] = getChild<LLButton>("mode_walk_btn");
	mModeControlButtonMap[MM_RUN] = getChild<LLButton>("mode_run_btn");
	mModeControlButtonMap[MM_FLY] = getChild<LLButton>("mode_fly_btn");
}

void LLMoveJoystick::initMovementMode()
{
	EMovementMode initMovementMode = gAgent.getAlwaysRun() ? MM_RUN : MM_WALK;
	if (gAgent.getFlying())
	{
		initMovementMode = MM_FLY;
	}
	setMovementMode(initMovementMode);

	if (isAgentAvatarValid())
	{
		showModeButtons(!gAgentAvatarp->isSitting());
	}
}

void LLMoveJoystick::setModeTooltip(const EMovementMode mode)
{
	llassert_always(mModeControlTooltipsMap.end() != mModeControlTooltipsMap.find(mode));
	control_tooltip_map_t controlsTipMap = mModeControlTooltipsMap[mode];
	control_tooltip_map_t::const_iterator it = controlsTipMap.begin();
	for (; it != controlsTipMap.end(); ++it)
	{
		LLView* ctrl = it->first;
		std::string tooltip = it->second;
		ctrl->setToolTip(tooltip);
	}
}

void LLMoveJoystick::setModeTitle(const EMovementMode mode)
{
	std::string title; 
	switch(mode)
	{
	case MM_WALK:
		title = getString("walk_title");
		break;
	case MM_RUN:
		title = getString("run_title");
		break;
	case MM_FLY:
		title = getString("fly_title");
		break;
	default:
		// title should be provided for all modes
		llassert(false);
		break;
	}
	setTitle(title);
}

//static
void LLMoveJoystick::sUpdateFlyingStatus()
{
	LLMoveJoystick *floater = LLFloaterReg::findTypedInstance<LLMoveJoystick>("left_joystick");
	if (floater) floater->mModeControlButtonMap[MM_FLY]->setEnabled(gAgent.canFly());
	
}

void LLMoveJoystick::showModeButtons(BOOL bShow)
{
	if (mModeActionsPanel->getVisible() == bShow)
		return;
	mModeActionsPanel->setVisible(bShow);
}

//static
void LLMoveJoystick::enableInstance(BOOL bEnable)
{
	LLMoveJoystick* instance = LLFloaterReg::findTypedInstance<LLMoveJoystick>("left_joystick");
	if (instance)
	{
		if (gAgent.getFlying())
		{
			instance->showModeButtons(FALSE);
		}
		else
		{
			instance->showModeButtons(bEnable);
		}
	}
}

void LLMoveJoystick::onOpen(const LLSD& key)
{
	if (gAgent.getFlying())
	{
		setFlyingMode(TRUE);
		showModeButtons(FALSE);
	}

	if (isAgentAvatarValid() && gAgentAvatarp->isSitting())
	{
		setSittingMode(TRUE);
		showModeButtons(FALSE);
	}

	sUpdateFlyingStatus();
}

void LLMoveJoystick::setModeButtonToggleState(const EMovementMode mode)
{
	llassert_always(mModeControlButtonMap.end() != mModeControlButtonMap.find(mode));

	mode_control_button_map_t::const_iterator it = mModeControlButtonMap.begin();
	for (; it != mModeControlButtonMap.end(); ++it)
	{
		it->second->setToggleState(FALSE);
	}

	mModeControlButtonMap[mode]->setToggleState(TRUE);
}