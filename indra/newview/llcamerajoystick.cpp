#include "llviewerprecompiledheaders.h"

#include "llcamerajoystick.h"

// Library includes
#include "llfloaterreg.h"

// Viewer includes
#include "llagentcamera.h"
#include "llfloatercamera.h"
#include "lljoystickbutton.h"
#include "llviewercontrol.h"
#include "llviewercamera.h"
#include "lltoolmgr.h"
#include "lltoolfocus.h"
#include "llslider.h"
#include "llfirstuse.h"
#include "llhints.h"

const F32 NUDGE_TIME = 0.25f;		// in seconds
const F32 ORBIT_NUDGE_RATE = 0.05f; // fraction of normal speed

// Constants
const F32 CAMERA_BUTTON_DELAY = 0.0f;

#define ORBIT "cam_rotate_stick"
#define PAN "cam_track_stick"
#define ZOOM "zoom"
#define PRESETS "preset_views_list"
#define CONTROLS "controls"

bool LLCameraJoystick::sFreeCamera = false;
bool LLCameraJoystick::sAppearanceEditing = false;

// Zoom the camera in and out
class LLPanelCameraZoom
:	public LLPanel
{
	LOG_CLASS(LLPanelCameraZoom);
public:
	LLPanelCameraZoom();

	/* virtual */ BOOL	postBuild();
	/* virtual */ void	draw();

protected:
	void	onZoomPlusHeldDown();
	void	onZoomMinusHeldDown();
	void	onSliderValueChanged();
	void	onCameraTrack();
	void	onCameraRotate();
	F32		getOrbitRate(F32 time);

private:
	LLButton*	mPlusBtn;
	LLButton*	mMinusBtn;
	LLSlider*	mSlider;
};

LLCameraJoystick* LLCameraJoystick::findInstance()
{
	return LLFloaterReg::findTypedInstance<LLCameraJoystick>("camera");
}

void LLCameraJoystick::onOpen(const LLSD& key)
{
	LLFirstUse::viewPopup();
	mClosed = FALSE;
}

void LLCameraJoystick::onClose(bool app_quitting)
{
	//We don't care of camera mode if app is quitting
	if(app_quitting)
		return;

	mClosed = TRUE;
}

LLCameraJoystick::LLCameraJoystick(const LLSD& val)
:	LLFloater(val),
	mClosed(FALSE)
{
	LLHints::registerHintTarget("view_popup", getHandle());
	mCommitCallbackRegistrar.add("CameraPresets.ChangeView", boost::bind(&LLCameraJoystick::onClickCameraItem, _2));
	setCanDrag(FALSE);
	LLFirstUse::viewPopup();
	mClosed = FALSE;
	setVisible(TRUE);
}

void LLCameraJoystick::onClickCameraItem(const LLSD& param)
{
	return;
}

void LLCameraJoystick::update()
{
	return;
}

void LLCameraJoystick::turnCamLeft()
{
	gAgentCamera.cameraPanLeft(-0.01);
}
void LLCameraJoystick::turnCamRight()
{
	gAgentCamera.cameraPanLeft(0.01);
}
void LLCameraJoystick::moveCamDown()
{
	gAgentCamera.cameraPanUp(-0.01);
}
void LLCameraJoystick::moveCamUp()
{
	gAgentCamera.cameraPanUp(0.01);
}
// virtual
BOOL LLCameraJoystick::postBuild()
{
	updateTransparency(TT_ACTIVE); // force using active floater transparency (STORM-730)

	mRotate = getChild<LLJoystickCameraRotate>(ORBIT);
	mTrack = getChild<LLJoystickCameraTrack>(PAN);

	/*
	mCamLeftButton = getChild<LLButton>("turn cam left btn"); 
	mCamLeftButton->setHeldDownCallback(boost::bind(&LLCameraJoystick::turnCamLeft, this));
	mCamRightButton = getChild<LLButton>("turn cam right btn"); 
	mCamRightButton->setHeldDownCallback(boost::bind(&LLCameraJoystick::turnCamRight, this));
	mCamUpButton = getChild<LLButton>("turn cam up btn"); 
	mCamUpButton->setHeldDownCallback(boost::bind(&LLCameraJoystick::moveCamUp, this));
	mCamDownButton = getChild<LLButton>("turn cam down btn"); 
	mCamDownButton->setHeldDownCallback(boost::bind(&LLCameraJoystick::moveCamDown, this));
	*/
	update();

	return LLFloater::postBuild();
}
