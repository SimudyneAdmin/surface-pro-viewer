#ifndef LLCAMERAJOYSTICK_H
#define LLCAMERAJOYSTICK_H

#include "llfloater.h"
#include "lliconctrl.h"
#include "lltextbox.h"
#include "llflatlistview.h"

class LLJoystickCameraRotate;
class LLJoystickCameraTrack;
class LLFloaterReg;

class LLCameraJoystick : public LLFloater
{
	friend class LLFloaterReg;
	
public:

	/* whether in free camera mode */
	static bool inFreeCameraMode();
	/* callback for camera items selection changing */
	static void onClickCameraItem(const LLSD& param);

	static void onLeavingMouseLook();

	/** resets current camera mode to orbit mode */
	static void resetCameraMode();

	/** Called when Avatar is entered/exited editing appearance mode */
	static void onAvatarEditingAppearance(bool editing);

	/* determines actual mode and updates ui */
	void update();

	/*switch to one of the camera presets (front, rear, side)*/
	static void switchToPreset(const std::string& name);

	/* move to CAMERA_CTRL_MODE_PRESETS from CAMERA_CTRL_MODE_FREE_CAMERA if we are on presets panel and
	   are not in free camera mode*/
	void fromFreeToPresets();

	virtual void onOpen(const LLSD& key);
	virtual void onClose(bool app_quitting);

	LLJoystickCameraRotate* mRotate;
	LLJoystickCameraTrack*	mTrack;

	LLButton*				mCamLeftButton;
	LLButton*				mCamRightButton;
	LLButton*				mCamDownButton;
	LLButton*				mCamUpButton;

	void turnCamLeft();
	void turnCamRight();
	void moveCamUp();
	void moveCamDown();

private:

	LLCameraJoystick(const LLSD& val);
	~LLCameraJoystick() {};

	/* return instance if it exists - created by LLFloaterReg */
	static LLCameraJoystick* findInstance();

	/*virtual*/ BOOL postBuild();

	/* update camera modes items selection and camera preset items selection according to the currently selected preset */
	void updateItemsSelection();
	
	// fills flatlist with items from given panel
	void fillFlatlistFromPanel (LLFlatListView* list, LLPanel* panel);

	// set to true when free camera mode is selected in modes list
	// remains true until preset camera mode is chosen, or pan button is clicked, or escape pressed
	static bool sFreeCamera;
	static bool sAppearanceEditing;
	BOOL mClosed;
};
#endif
